package com.example.myprogram.batteries;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.example.myprogram.R;

public class AddBatteries extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_batteries);
    }

    public void proceed(View view){

        EditText name,manufacturer,capacity,price;

        name = findViewById(R.id.addBatteriesName);
        manufacturer = findViewById(R.id.addBatteriesManufacturer);
        capacity = findViewById(R.id.addBatteriesCapacity);
        price = findViewById(R.id.addBatteriesPrice);

        BatteriesValues batteriesValues = new BatteriesValues(name.getText().toString(),
                manufacturer.getText().toString(),
                R.drawable.your_photo_here,
                Integer.parseInt(capacity.getText().toString()),
                Integer.parseInt(price.getText().toString()));

        Intent intent = new Intent();
        intent.putExtra(BatteriesModels.ITEM_KEY,batteriesValues);
        setResult(RESULT_OK,intent);

        onBackPressed();
    }

}
