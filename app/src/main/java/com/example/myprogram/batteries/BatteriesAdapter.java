package com.example.myprogram.batteries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myprogram.R;

import java.util.List;

public class BatteriesAdapter extends ArrayAdapter {

    private List<BatteriesValues> batteriesValuesList;

    private LayoutInflater layoutInflater;

    public BatteriesAdapter(Context context, List objects){
        super(context, R.layout.batteries_item_list, objects);

        batteriesValuesList = objects;

        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.batteries_item_list,parent,false);
        }

        ImageView imageView = convertView.findViewById(R.id.batteriesPhoto);
        TextView nameView = convertView.findViewById(R.id.batteriesName);
        TextView manufacturerView = convertView.findViewById(R.id.batteriesManufacturer);
        TextView capacityView = convertView.findViewById(R.id.batteriesCapacity);
        TextView priceView = convertView.findViewById(R.id.batteriesPrice);

        BatteriesValues objectBoxMode = batteriesValuesList.get(position);

        imageView.setImageResource(batteriesValuesList.get(position).getBatteriesPhotoId());
        nameView.setText("Name: "+objectBoxMode.getBatteriesName());
        manufacturerView.setText("Manufacturer: "+objectBoxMode.getBatteriesManufacturer());
        capacityView.setText("Capacity: "+Integer.toString(objectBoxMode.getBatteriesCapacity()));
        priceView.setText("Price: "+Double.toString(objectBoxMode.getBatteriesPrice()));

        return convertView;
    }


}
