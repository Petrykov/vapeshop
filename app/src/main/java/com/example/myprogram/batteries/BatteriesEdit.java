package com.example.myprogram.batteries;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myprogram.R;
import com.example.myprogram.boxModes.BoxModesKitsModels;
import com.example.myprogram.boxModes.BoxModesValues;

import java.util.ArrayList;
import java.util.List;

public class BatteriesEdit extends AppCompatActivity {

    private EditText batteriesName;
    private EditText batteriesManufacturer;
    private EditText batteriesCapacity;
    private EditText batteriesPrice;

    private Button submit;

    private int photoResourseId;

    BatteriesValues batteriesValues;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batteries_edit);

        batteriesName = findViewById(R.id.batteriesNameEdit);
        batteriesManufacturer = findViewById(R.id.batteriesManufacturerEdit);
        batteriesCapacity = findViewById(R.id.batteriesCapacityEdit);
        batteriesPrice = findViewById(R.id.batteriesPriceEdit);


        submit = findViewById(R.id.submitChangesEdit);

        final Intent intent = getIntent();
        final BatteriesValues batteriesValues = intent.getExtras().getParcelable(BoxModesKitsModels.ITEM_KEY);

        batteriesName.setText(batteriesValues.getBatteriesName());
        batteriesManufacturer.setText(batteriesValues.getBatteriesManufacturer());
        batteriesCapacity.setText("" + batteriesValues.getBatteriesCapacity());
        batteriesPrice.setText("" + batteriesValues.getBatteriesPrice());
        photoResourseId = batteriesValues.getBatteriesPhotoId();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BatteriesValues changedBatteriesValue = new BatteriesValues(batteriesName.getText().toString(), batteriesManufacturer.getText().toString(), photoResourseId, Integer.parseInt(batteriesCapacity.getText().toString()), Integer.parseInt(batteriesPrice.getText().toString()));

                Intent intent_second = new Intent();
                intent_second.putExtra(BatteriesModels.ITEM_KEY, changedBatteriesValue);
                setResult(RESULT_OK, intent_second);

                onBackPressed();
            }
        });

    }
}


