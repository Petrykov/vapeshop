package com.example.myprogram.batteries;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.myprogram.R;
import com.example.myprogram.boxModes.BoxModeEdit;
import com.example.myprogram.boxModes.BoxModesDescription;
import com.example.myprogram.boxModes.BoxModesKitsModels;
import com.example.myprogram.boxModes.BoxModesValues;

import java.util.ArrayList;
import java.util.List;

public class BatteriesModels extends AppCompatActivity {

    private List<BatteriesValues> batteriesModelsList;

    public static final String ITEM_KEY = "hello";
    public static final int ACTION_ADD_BATTERIES = 115;
    public static final int ACTION_EDIT_BATTERIES = 116;

    private boolean attempToDelete = false;
    public  boolean attemptToEdit = false;

    private int positionDeletedItem = -1;
    private int editBatteries;

    private BatteriesValues deletedItem = null;

    BatteriesAdapter batteriesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batteries_models);

        batteriesModelsList = new ArrayList<>();

        batteriesModelsList.add(new BatteriesValues("25 R","Samsung",R.drawable.samsung_25_r,2500,7));
        batteriesModelsList.add(new BatteriesValues("30 Q","Samsung",R.drawable.samsung_30q,3000,8));
        batteriesModelsList.add(new BatteriesValues("VTC 6","SONY",R.drawable.sony_vtc6,3000,8));
        batteriesModelsList.add(new BatteriesValues("LG","HG2",R.drawable.lg_hg2,3000,7));
        batteriesModelsList.add(new BatteriesValues("VTC 5A","Sony",R.drawable.vtc_fivea,2100,8));

        ListView batteriesModelsListView = findViewById(R.id.batteriesListView);
        batteriesAdapter = new BatteriesAdapter(this,batteriesModelsList);
        batteriesModelsListView.setAdapter(batteriesAdapter);

        batteriesModelsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (attempToDelete) {
                    deletedItem = batteriesModelsList.get(position);
                    positionDeletedItem = position;
                    batteriesModelsList.remove(position);
                    batteriesAdapter.notifyDataSetChanged();

                    Snackbar.make(view,"The " + deletedItem.getBatteriesName() + " was successfully deleted!",Snackbar.LENGTH_LONG).setAction("Return", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            batteriesModelsList.add(positionDeletedItem,deletedItem);
                            batteriesAdapter.notifyDataSetChanged();
                        }
                    }).show();

                    attempToDelete = false;
                }else{
                    if(attemptToEdit){
                        Intent intent = new Intent (BatteriesModels.this, BatteriesEdit.class);
                        intent.putExtra(ITEM_KEY,batteriesModelsList.get(position));
                        editBatteries = position;
                        attemptToEdit = false;
                        startActivityForResult(intent,ACTION_EDIT_BATTERIES);
                    }
                    else {
                        Intent intent = new Intent(BatteriesModels.this, BoxModesDescription.class);
                        intent.putExtra(ITEM_KEY, batteriesModelsList.get(position));

                        startActivity(intent);
                    }
                }
            }
        });

        Button delete_button = findViewById(R.id.delete_button_batteries);
        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to delete",Snackbar.LENGTH_LONG).show();
                attempToDelete = true;
            }
        });

        Button editButton = findViewById(R.id.edit_button_batteries);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to edit",Snackbar.LENGTH_LONG).show();
                attemptToEdit = true;
            }
        });

    }

    public void addBatterie(View view){
        startActivityForResult(new Intent(this,AddBatteries.class),ACTION_ADD_BATTERIES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == ACTION_ADD_BATTERIES & resultCode == RESULT_OK){
            BatteriesValues batteriesValues = data.getExtras().getParcelable(ITEM_KEY);
            batteriesModelsList.add(batteriesValues);
            batteriesAdapter    .notifyDataSetChanged();
        }else if (requestCode == ACTION_EDIT_BATTERIES & resultCode == RESULT_OK){
            BatteriesValues batteriesValues = data.getExtras().getParcelable(ITEM_KEY);
            batteriesModelsList.set(editBatteries,batteriesValues);
            batteriesAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
