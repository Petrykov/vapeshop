package com.example.myprogram.batteries;

import android.os.Parcel;
import android.os.Parcelable;

public class BatteriesValues implements Parcelable {

    private String batteriesName;
    private String batteriesManufacturer;

    private int batteriesPhotoId;
    private int batteriesCapacity;
    private int batteriesPrice;

    public BatteriesValues(String batteriesName, String batteriesManufacturer, int batteriesPhotoId, int batteriesCapacity, int batteriesPrice) {
        this.batteriesName = batteriesName;
        this.batteriesManufacturer = batteriesManufacturer;
        this.batteriesPhotoId = batteriesPhotoId;
        this.batteriesCapacity = batteriesCapacity;
        this.batteriesPrice = batteriesPrice;
    }


    public String getBatteriesName() {
        return batteriesName;
    }

    public String getBatteriesManufacturer() {
        return batteriesManufacturer;
    }

    public int getBatteriesPhotoId() {
        return batteriesPhotoId;
    }

    public int getBatteriesCapacity() {
        return batteriesCapacity;
    }

    public int getBatteriesPrice() {
        return batteriesPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.batteriesName);
        dest.writeString(this.batteriesManufacturer);
        dest.writeInt(this.batteriesPhotoId);
        dest.writeInt(this.batteriesCapacity);
        dest.writeInt(this.batteriesPrice);
    }

    protected BatteriesValues(Parcel in) {
        this.batteriesName = in.readString();
        this.batteriesManufacturer = in.readString();
        this.batteriesPhotoId = in.readInt();
        this.batteriesCapacity = in.readInt();
        this.batteriesPrice = in.readInt();
    }

    public static final Creator<BatteriesValues> CREATOR = new Creator<BatteriesValues>() {
        @Override
        public BatteriesValues createFromParcel(Parcel source) {
            return new BatteriesValues(source);
        }

        @Override
        public BatteriesValues[] newArray(int size) {
            return new BatteriesValues[size];
        }
    };
}
