package com.example.myprogram.boxModes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myprogram.batteries.BatteriesValues;
import com.example.myprogram.R;

public class AddBatteriesToBoxMode extends AppCompatActivity {

    private EditText name;
    private EditText manufacturer;
    private EditText capacity;
    private EditText price;

    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_batteries_to_box_mode);

        name = findViewById(R.id.newNameBattery);
        manufacturer = findViewById(R.id.newManufacBattery);
        capacity = findViewById(R.id.newCapacityBatttery);
        price = findViewById(R.id.newPriceBattery);
        add = findViewById(R.id.addNewBattery);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameBattery = name.getText().toString();
                String manufacturerBattery = manufacturer.getText().toString();
                int capacityBattery = Integer.parseInt(capacity.getText().toString());
                int priceBattery = Integer.parseInt(price.getText().toString());

                BatteriesValues batteriesValues = new BatteriesValues(nameBattery,manufacturerBattery,R.drawable.your_photo_here,capacityBattery,priceBattery);

                Intent intent = new Intent();
                intent.putExtra(BoxModesKitsModels.ITEM_KEY,batteriesValues);
                setResult(RESULT_OK,intent);

                onBackPressed();
            }
        });

    }
}
