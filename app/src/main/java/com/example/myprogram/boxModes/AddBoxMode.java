package com.example.myprogram.boxModes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.myprogram.R;

public class AddBoxMode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_box_mode);
    }

    public void proceed(View view){

        EditText name,manufacturer,batteries,price,power;

        name = findViewById(R.id.addBoxModeName);
        manufacturer = findViewById(R.id.addBoxModeManufacturer);
        batteries = findViewById(R.id.addBoxModeBatteries);
        price = findViewById(R.id.addBoxModePrice);
        power = findViewById(R.id.addBoxModePower);

        BoxModesValues boxmode = new BoxModesValues(name.getText().toString(),manufacturer.getText().toString(),-1,R.drawable.your_photo_here,Integer.parseInt(batteries.getText().toString()),Integer.parseInt(power.getText().toString()),Integer.parseInt(price.getText().toString()));

        Intent intent = new Intent();
        intent.putExtra(BoxModesKitsModels.ITEM_KEY,boxmode);
        setResult(RESULT_OK,intent);

        onBackPressed();
    }
}
