package com.example.myprogram.boxModes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myprogram.batteries.BatteriesValues;
import com.example.myprogram.R;

import java.util.ArrayList;
import java.util.List;

public class BoxModeEdit extends AppCompatActivity {

    private EditText boxManufacturer;
    private EditText boxName;
    private EditText boxPrice;
    private EditText boxBatteries;
    private EditText boxPower;

    private Button submit;

    private int photoResourseId;
    private int descriptionId;

    private List<BatteriesValues> list;



    BoxModesValues boxModes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_mode_edit);

        boxManufacturer = findViewById(R.id.boxManufacturer);
        boxName =  findViewById(R.id.boxName);
        boxPrice  = findViewById(R.id.boxPrice);
        boxBatteries = findViewById(R.id.boxBatteries);
        boxPower = findViewById(R.id.boxPower);
        submit = findViewById(R.id.submitChanges);

        final Intent intent = getIntent();
        final BoxModesValues thisBoxMode = intent.getExtras().getParcelable(BoxModesKitsModels.ITEM_KEY);

        boxManufacturer.setText(thisBoxMode.getManufacturer());
        boxName.setText(thisBoxMode.getName());
        boxPrice.setText(""+thisBoxMode.getPrice());
        boxBatteries.setText(""+thisBoxMode.getBatteriesRequired());
        boxPower.setText(""+thisBoxMode.getMaxPower());
        photoResourseId = thisBoxMode.photoId;
        descriptionId = thisBoxMode.description;
        list = new ArrayList<>(thisBoxMode.batteriesValuesList);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BoxModesValues boxMode = new BoxModesValues(boxName.getText().toString(),boxManufacturer.getText().toString(),descriptionId,photoResourseId,Integer.parseInt(boxBatteries.getText().toString()),Integer.parseInt(boxPower.getText().toString()),Integer.parseInt(boxPrice.getText().toString()),list);

                Intent intent_second = new Intent();
                intent_second.putExtra(BoxModesKitsModels.ITEM_KEY,boxMode);
                setResult(RESULT_OK,intent_second);

                onBackPressed();
            }
        });

    }
}
