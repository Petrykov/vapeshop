package com.example.myprogram.boxModes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myprogram.R;

import java.util.List;

public class BoxModesAdapter extends ArrayAdapter {

    private List<BoxModesValues> boxModesValues;

    private LayoutInflater layoutInflater;

    public BoxModesAdapter(Context context, List objects){
        super(context, R.layout.box_modes_item_list, objects);

        boxModesValues = objects;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.box_modes_item_list,parent,false);
        }

        ImageView imageView = convertView.findViewById(R.id.imageBox);
        TextView nameView = convertView.findViewById(R.id.nameBox);
        TextView manufacturerView = convertView.findViewById(R.id.manufacturerBox);
        TextView batteriesView = convertView.findViewById(R.id.batteriesBox);
        TextView powerView = convertView.findViewById(R.id.powerBox);
        TextView priceView = convertView.findViewById(R.id.priceBox);

        BoxModesValues objectBoxMode = boxModesValues.get(position);

        if(objectBoxMode.getPhotoId() != -1) imageView.setImageResource(boxModesValues.get(position).getPhotoId());


        nameView.setText("Name: "+objectBoxMode.getName());
        manufacturerView.setText("Manufacturer: "+objectBoxMode.getManufacturer());
        batteriesView.setText("Battaries: "+Integer.toString(objectBoxMode.getBatteriesRequired()));
        powerView.setText("Power: "+Integer.toString(objectBoxMode.getMaxPower()));
        priceView.setText("Price: "+Double.toString(objectBoxMode.getPrice()));

        return convertView;
    }

}
