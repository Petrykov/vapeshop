package com.example.myprogram.boxModes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myprogram.batteries.BatteriesAdapter;
import com.example.myprogram.batteries.BatteriesValues;
import com.example.myprogram.R;
import com.example.myprogram.onClickButtons.OnClickCallButton;

import java.util.ArrayList;
import java.util.List;

public class BoxModesDescription extends AppCompatActivity {

    private int deleteItem = -1;
    private int editItem = -1;

    private boolean attempToDelete = false;
    private boolean attemToEdit = false;

    private ListView listView;
    private List<BatteriesValues> batteriesValuesList;

    private BatteriesAdapter batteriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_modes_description);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.contactInfoButton);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BoxModesDescription.this, OnClickCallButton.class));
            }
        });

        Intent intent = getIntent();

        BoxModesValues boxModesValues = intent.getExtras().getParcelable(BoxModesKitsModels.ITEM_KEY);
        getSupportActionBar().setTitle(boxModesValues.getName());

        TextView textView = findViewById(R.id.outputTextView);

        if(boxModesValues.getDescription() != -1) textView.setText(boxModesValues.getDescription());


        listView = findViewById(R.id.outputListView);
        batteriesValuesList = new ArrayList<>(boxModesValues.batteriesValuesList);

        batteriesAdapter = new BatteriesAdapter(this,batteriesValuesList);

        listView.setAdapter(batteriesAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(attempToDelete){

                    batteriesValuesList.remove(position);
                    batteriesAdapter.notifyDataSetChanged();

                    attempToDelete = false;
                } else if(attemToEdit){

                    Intent bah = new Intent(BoxModesDescription.this,EditBatteriesToBoxMode.class);
                    bah.putExtra(BoxModesKitsModels.ITEM_KEY,batteriesValuesList.get(position));

                    editItem = position;
                    attemToEdit = false;

                    startActivityForResult(bah,56);

                }
            }
        });

        Button deleteBtn = findViewById(R.id.DELETE);
        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Snackbar.make(v,"Press on item to delete",Snackbar.LENGTH_SHORT).show();
                attempToDelete = true;
            }
        });

        Button addBtn = findViewById(R.id.ADD);
        addBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(BoxModesDescription.this,AddBatteriesToBoxMode.class);
                startActivityForResult(intentAdd,55);
            }
        });

        Button editBtn = findViewById(R.id.EDIT);
        editBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                attemToEdit = true;
                Toast.makeText(BoxModesDescription.super.getBaseContext(),"Press on item to edit...",Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == 55 & resultCode == RESULT_OK){

            BatteriesValues batareyka = data.getExtras().getParcelable(BoxModesKitsModels.ITEM_KEY);

            batteriesValuesList.add(batareyka);
            batteriesAdapter.notifyDataSetChanged();

        } else if (requestCode == 56 & resultCode == RESULT_OK){

            String nameS = data.getStringExtra(EditBatteriesToBoxMode.NAME_KEY);
            String manufacturerS = data.getStringExtra(EditBatteriesToBoxMode.MANUFACTURER_KEY);
            int capacityS =  data.getIntExtra(EditBatteriesToBoxMode.CAPACITY_KEY,-1);
            int priceS = data.getIntExtra(EditBatteriesToBoxMode.PRICE_KEY,-1);
            int photoID = data.getIntExtra(EditBatteriesToBoxMode.PHOTO_KEY,R.drawable.your_photo_here);

            BatteriesValues batareyka = new BatteriesValues(nameS,manufacturerS,photoID,capacityS,priceS);

            batteriesValuesList.set(editItem,batareyka);
            batteriesAdapter.notifyDataSetChanged();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
