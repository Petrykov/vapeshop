package com.example.myprogram.boxModes;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.myprogram.R;

import java.util.ArrayList;
import java.util.List;

public class BoxModesKitsModels extends AppCompatActivity {

    private List <BoxModesValues> boxModesKitsList;

    public static final String ITEM_KEY = "hello";
    public static final int ACTION_ADD_BOX_MODE = 115;
    public static final int ACTION_EDIT_BOX_MODE = 116;

    private int editBoxMode;
    private int positionDeletedItem = -1;

    private boolean attempToDelete = false;
    public  boolean attemptToEdit = false;

    BoxModesAdapter boxModesAdapter;

    private BoxModesValues deletedItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_modes_kits);

        boxModesKitsList = new ArrayList<>();

        boxModesKitsList.add(new BoxModesValues("DRAG","USA",R.string.drag_description,R.drawable.drag,2,157,70));
        boxModesKitsList.add(new BoxModesValues("RX 2/3","USA",R.string.rx_23_description,R.drawable.rx_23,3,250,60));
        boxModesKitsList.add(new BoxModesValues("ElPico","RUSSIA",R.string.eleaf_pico_description,R.drawable.eleaf_pico,1,75,30));
        boxModesKitsList.add(new BoxModesValues("Fuchai ","USA",R.string.fuchai_213_description,R.drawable.fuchai_213,2,213,75));
        boxModesKitsList.add(new BoxModesValues("Tesla ","USA",R.string.tesla_pung_description,R.drawable.tesla_pung_220w,2,220,65));
        boxModesKitsList.add(new BoxModesValues("Wye","USA",R.string.tesla_wye_description,R.drawable.tesla_wye,2,250,60));
        boxModesKitsList.add(new BoxModesValues("Minikin","USA",R.string.minikin_vt_description,R.drawable.minikin_vt,2,250,70));

        //Batteries for DRAG
        boxModesKitsList.get(0).addBatteries("VTC 5A","Sony",R.drawable.vtc_fivea,2100,8);
        boxModesKitsList.get(0).addBatteries("25 R","Samsung",R.drawable.samsung_25_r,2500,7);
        //Batteries for RX 2/3
        boxModesKitsList.get(1).addBatteries("30 Q","Samsung",R.drawable.samsung_30q,3000,8);
        boxModesKitsList.get(1).addBatteries("25 R","Samsung",R.drawable.samsung_25_r,2500,7);
        //Batteries for ElPico
        boxModesKitsList.get(2).addBatteries("LG","HG2",R.drawable.lg_hg2,3000,7);
        boxModesKitsList.get(2).addBatteries("30q","Samsung",R.drawable.samsung_30q,3000,8);
        //Batteries for Fuchai
        boxModesKitsList.get(3).addBatteries("VTC 6","Sony",R.drawable.sony_vtc6,3000,8);
        boxModesKitsList.get(3).addBatteries("VTC 5A","Sony",R.drawable.vtc_fivea,2100,8);
        //Batteries for Tesla
        boxModesKitsList.get(4).addBatteries("LG","HG2",R.drawable.lg_hg2,3000,7);
        boxModesKitsList.get(4).addBatteries("25 R","Samsung",R.drawable.samsung_25_r,2500,7);
        //Batteries for Tesla Wye
        boxModesKitsList.get(5).addBatteries("LG","HG2",R.drawable.lg_hg2,3000,7);
        boxModesKitsList.get(5).addBatteries("30 Q","Samsung",R.drawable.samsung_30q,3000,8);
        //Batteries for Minikin V2
        boxModesKitsList.get(6).addBatteries("VTC 6","Sony",R.drawable.sony_vtc6,3000,8);
        boxModesKitsList.get(6).addBatteries("VTC 5A","Sony",R.drawable.vtc_fivea,2100,8);

        ListView boxModesKitsListView = findViewById(R.id.batteriesListView);

        boxModesAdapter = new BoxModesAdapter(this,boxModesKitsList);
        boxModesKitsListView.setAdapter(boxModesAdapter);

        boxModesKitsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (attempToDelete) {
                    deletedItem = boxModesKitsList.get(position);
                    positionDeletedItem = position;
                    boxModesKitsList.remove(position);
                    boxModesAdapter.notifyDataSetChanged();

                    Snackbar.make(view,"The " + deletedItem.getName() + " was successfully deleted!",Snackbar.LENGTH_LONG).setAction("Return", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boxModesKitsList.add(positionDeletedItem,deletedItem);
                            boxModesAdapter.notifyDataSetChanged();
                        }
                    }).show();

                    attempToDelete = false;
                } else {
                    if(attemptToEdit){
                        Intent intent = new Intent (BoxModesKitsModels.this,BoxModeEdit.class);
                        intent.putExtra(ITEM_KEY,boxModesKitsList.get(position));
                        editBoxMode = position;
                        attemptToEdit = false;
                        startActivityForResult(intent,ACTION_EDIT_BOX_MODE);

                    }
                    else {
                        Intent intent = new Intent(BoxModesKitsModels.this, BoxModesDescription.class);
                        intent.putExtra(ITEM_KEY, boxModesKitsList.get(position));

                        startActivity(intent);
                    }
                }

            }
        });

        Button delete_button = findViewById(R.id.delete_button2);

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to delete",Snackbar.LENGTH_LONG).show();
                attempToDelete = true;
            }
        });

        Button editButton = findViewById(R.id.delete_button_batteries);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to edit",Snackbar.LENGTH_LONG).show();
                attemptToEdit = true;
            }
        });

    }

    public void add(View view){
        startActivityForResult(new Intent(this,AddBoxMode.class),ACTION_ADD_BOX_MODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == ACTION_ADD_BOX_MODE & resultCode == RESULT_OK){
            BoxModesValues boxMode = data.getExtras().getParcelable(ITEM_KEY);
            boxModesKitsList.add(boxMode);
            boxModesAdapter.notifyDataSetChanged();
        } else if (requestCode == ACTION_EDIT_BOX_MODE & resultCode == RESULT_OK){
            BoxModesValues boxModesValues = data.getExtras().getParcelable(ITEM_KEY);
            boxModesKitsList.set(editBoxMode,boxModesValues);
            boxModesAdapter.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
