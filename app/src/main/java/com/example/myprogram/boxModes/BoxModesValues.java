package com.example.myprogram.boxModes;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myprogram.batteries.BatteriesValues;

import java.util.ArrayList;
import java.util.List;

public class BoxModesValues implements Parcelable {

    public String name;
    public String manufacturer;

    public int description;
    public int photoId;
    public int batteriesRequired;
    public int maxPower;
    public int price;

    public List<BatteriesValues> batteriesValuesList;

    public BoxModesValues(String name, String manufacturer,int description, int photoId, int batteriesRequired, int maxPower, int price ){
        this.name = name;
        this.manufacturer = manufacturer;
        this.description = description;
        this.photoId = photoId;
        this.batteriesRequired = batteriesRequired;
        this.maxPower = maxPower;
        this.price = price;
        batteriesValuesList = new ArrayList<>();
    }

    public BoxModesValues(String name, String manufacturer,int description, int photoId, int batteriesRequired, int maxPower, int price, List<BatteriesValues> list){
        this.name = name;
        this.manufacturer = manufacturer;
        this.description = description;
        this.photoId = photoId;
        this.batteriesRequired = batteriesRequired;
        this.maxPower = maxPower;
        this.price = price;
        batteriesValuesList = new ArrayList<>(list);

    }

    public BoxModesValues(String name, String manufacturer, int photoId, int batteriesRequired, int maxPower, int price) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.photoId = photoId;
        this.batteriesRequired = batteriesRequired;
        this.maxPower = maxPower;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getDescription() {
        return description;
    }

    public int getPhotoId() {
        return photoId;
    }

    public int getBatteriesRequired() {
        return batteriesRequired;
    }

    public int getMaxPower() {
        return maxPower;
    }

    public int getPrice() {
        return price;
    }

    public List<BatteriesValues> getBatteriesValuesList() {
        return batteriesValuesList;
    }

    public void addBatteries(String batteriesMame, String batteriesManufacturer, int batteriesId, int batteriesCapacity, int batteriesPrice){
        batteriesValuesList.add(new BatteriesValues(batteriesMame,batteriesManufacturer,batteriesId,batteriesCapacity,batteriesPrice));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.manufacturer);
        dest.writeInt(this.description);
        dest.writeInt(this.photoId);
        dest.writeInt(this.batteriesRequired);
        dest.writeInt(this.maxPower);
        dest.writeInt(this.price);
        dest.writeList(this.batteriesValuesList);
    }

    protected BoxModesValues(Parcel in) {
        this.name = in.readString();
        this.manufacturer = in.readString();
        this.description = in.readInt();
        this.photoId = in.readInt();
        this.batteriesRequired = in.readInt();
        this.maxPower = in.readInt();
        this.price = in.readInt();
        this.batteriesValuesList = new ArrayList<BatteriesValues>();
        in.readList(this.batteriesValuesList, BatteriesValues.class.getClassLoader());
    }

    public static final Parcelable.Creator<BoxModesValues> CREATOR = new Parcelable.Creator<BoxModesValues>() {
        @Override
        public BoxModesValues createFromParcel(Parcel source) {
            return new BoxModesValues(source);
        }

        @Override
        public BoxModesValues[] newArray(int size) {
            return new BoxModesValues[size];
        }
    };
}

