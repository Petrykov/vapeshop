package com.example.myprogram.boxModes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myprogram.batteries.BatteriesValues;
import com.example.myprogram.R;

public class EditBatteriesToBoxMode extends AppCompatActivity {

    public static final String NAME_KEY = "name";
    public static final String MANUFACTURER_KEY = "manuha";
    public static final String PHOTO_KEY = "photohey";
    public static final String CAPACITY_KEY = "capat";
    public static final String PRICE_KEY = "money-money-money :))";

    private EditText name;
    private EditText manufacturer;
    private EditText capacity;
    private EditText price;

    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_batteries_to_box_mode);

        name = findViewById(R.id.newNameBattery);
        manufacturer = findViewById(R.id.newManufacBattery);
        capacity = findViewById(R.id.newCapacityBatttery);
        price = findViewById(R.id.newPriceBattery);
        add = findViewById(R.id.addNewBattery);

        Intent intent = getIntent();

        BatteriesValues batareyka = intent.getExtras().getParcelable(BoxModesKitsModels.ITEM_KEY);

        final int photoId = batareyka.getBatteriesPhotoId();

        name.setText(batareyka.getBatteriesName());
        manufacturer.setText(batareyka.getBatteriesManufacturer());
        capacity.setText("" + batareyka.getBatteriesCapacity());
        price.setText("" + batareyka.getBatteriesPrice());
        add.setText("Edit");

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BatteriesValues batareyka = new BatteriesValues(name.getText().toString(),manufacturer.getText().toString(),photoId,Integer.parseInt(capacity.getText().toString()),Integer.parseInt(price.getText().toString()));

                String nameS = name.getText().toString();
                String manufacturerS = manufacturer.getText().toString();
                int capacityS = Integer.parseInt(capacity.getText().toString());
                int priceS = Integer.parseInt(price.getText().toString());

                Intent intent = new Intent();
                intent.putExtra(NAME_KEY,nameS);
                intent.putExtra(MANUFACTURER_KEY,manufacturerS);
                intent.putExtra(PHOTO_KEY,photoId);
                intent.putExtra(CAPACITY_KEY,capacityS);
                intent.putExtra(PRICE_KEY,priceS);
                setResult(RESULT_OK,intent);

                onBackPressed();
            }
        });

        TextView greetings = findViewById(R.id.textView);

        greetings.setText(getString(R.string.edit_battery_info));

    }
}
