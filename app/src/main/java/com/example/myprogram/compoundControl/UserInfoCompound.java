package com.example.myprogram.compoundControl;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myprogram.R;

public class UserInfoCompound extends LinearLayout {

   TextView generalInfo;
   TextView modeName;
   TextView modeManufacturer;
   TextView modePower;
   TextView modeBatteries;
   TextView modePrice;

   Button addButton;

    public UserInfoCompound(Context context) {
        super(context);
        init();
    }

    public UserInfoCompound(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserInfoCompound(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UserInfoCompound(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init(){
        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.user_info_view, this);

        generalInfo = findViewById(R.id.enterYourDeviceInfo);
        modeName = findViewById(R.id.addBoxModeName);
        modeManufacturer = findViewById(R.id.addBoxModeManufacturer);
        modePower = findViewById(R.id.addBoxModePower);
        modeBatteries = findViewById(R.id.addBoxModeBatteries);
        modePrice = findViewById(R.id.addBoxModePrice);
        addButton = findViewById(R.id.addNew);

    }

}
