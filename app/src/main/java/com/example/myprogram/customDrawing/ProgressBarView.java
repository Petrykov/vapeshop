package com.example.myprogram.customDrawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class ProgressBarView extends View {
    Paint backgroundPaint, foreground;
    int value = 0, maxValue = Integer.MAX_VALUE ;
    public ProgressBarView(Context context) {
        super(context);
        innit();
    }

    public ProgressBarView(Context context,  AttributeSet attrs) {
        super(context, attrs);
        innit();
    }

    public ProgressBarView(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        innit();
    }

    public ProgressBarView(Context context,  AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        innit();
    }

    private void innit(){
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.TRANSPARENT);
        backgroundPaint.setStyle(Paint.Style.FILL);

        foreground = new Paint();
        foreground.setColor(Color.YELLOW);
    }

    public void setValue(int value) {
        if (value>=0 && value<=maxValue) {
            this.value = value;
        }

        Log.d(getClass().getCanonicalName(),"value "+ value);

        invalidate();
    }

    public void setMaxValue(int maxValue) {
        if (maxValue>0) {
            this.maxValue = maxValue;
        }
    }

    public int getValue() {
        return value;
    }

    public int getMaxValue() {
        return maxValue;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth(), height = canvas.getHeight();

        canvas.drawRect(0,0,width-1,height-1,backgroundPaint);
        canvas.drawRect(0,0,width * value/maxValue,height - 1,foreground);
    }
}
