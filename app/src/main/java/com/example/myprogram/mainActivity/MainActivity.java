package com.example.myprogram.mainActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myprogram.batteries.BatteriesModels;
import com.example.myprogram.boxModes.BoxModesKitsModels;
import com.example.myprogram.contactUs.ContactUsModels;
import com.example.myprogram.generalInfo.GeneralInfoModels;
import com.example.myprogram.R;
import com.example.myprogram.rtaRda.RtaRdaModels;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToGeneralInfo(View view){
        startActivity(new Intent(this, GeneralInfoModels.class));
    }

    public void goToContactUs(View view){
        startActivity(new Intent(this, ContactUsModels.class));
    }

    public void goToRtaRda(View view){
        startActivity(new Intent(this, RtaRdaModels.class));
    }

    public void goToBoxModes(View view){
        startActivity(new Intent(this, BoxModesKitsModels.class));
    }

    public void goToBatteries(View view){
        startActivity(new Intent(this,BatteriesModels.class));
    }

}
