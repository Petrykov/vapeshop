package com.example.myprogram.onClickButtons;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.myprogram.customDrawing.ProgressBarView;
import com.example.myprogram.R;

import java.util.Timer;
import java.util.TimerTask;

public class OnClickCallButton extends AppCompatActivity {

    private String name;
    private String surName;
    private String phoneNumber;

    ProgressBarView ppbTest;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_click_call_button);

    }

    public void submitButton(View view) {

        ppbTest = findViewById(R.id.progressBarView);
        ppbTest.setMaxValue(700);
        ppbTest.setValue(0);

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                int currentValue = ppbTest.getValue();

                if (currentValue == ppbTest.getMaxValue()) {
//                    currentValue = 0;
                    timer.cancel();
                } else {
                    currentValue++;
                }
                textView = findViewById(R.id.textViewMessage);
                textView.setText("   Information received, we will contact you soon!");
                ppbTest.setValue(currentValue);
                Log.d("test", "" + currentValue);

            }
        }, 0, 2);

    }
}
