package com.example.myprogram.rtaRda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.myprogram.R;

public class AddRtaRda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rta_rda);
    }

    public void proceed(View view){

        EditText name,manufacturer,type,coils,price;

        name = findViewById(R.id.addRtaRdaName);
        manufacturer = findViewById(R.id.addRtaRdaManufacturer);
        type = findViewById(R.id.addRtaRdaType);
        coils = findViewById(R.id.addRtaRdaCoils);
        price = findViewById(R.id.addRtaRdaPrice);

        String str = price.getText().toString();

        RtaRdaValues rtarda = new RtaRdaValues(name.getText().toString(),
                manufacturer.getText().toString(),
                type.getText().toString(),
                Integer.parseInt(coils.getText().toString()),
                Double.parseDouble(str),R.drawable.your_photo_here,-1);

        Intent intent = new Intent();
        intent.putExtra(RtaRdaModels.ITEM_KEY,rtarda);
        setResult(RESULT_OK,intent);

        onBackPressed();
    }

}
