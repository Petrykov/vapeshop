package com.example.myprogram.rtaRda;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myprogram.R;

import java.util.List;

public class RtaRdaAdapter extends ArrayAdapter {


    private List <RtaRdaValues> rtaRdaValues;

    private LayoutInflater layoutInflater;

    public RtaRdaAdapter(Context context, List objects){
        super(context,R.layout.rta_rda_item_list, objects);

        rtaRdaValues = objects;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.rta_rda_item_list,parent,false);
        }

        ImageView imageView = convertView.findViewById(R.id.object_photo);
        TextView manufacturerView = convertView.findViewById(R.id.object_manufacturer);
        TextView nameView = convertView.findViewById(R.id.object_name);
        TextView typeView = convertView.findViewById(R.id.object_type);
        TextView coilsView = convertView.findViewById(R.id.object_coils);
        TextView priceView = convertView.findViewById(R.id.object_price);

        RtaRdaValues objectRtaRda = rtaRdaValues.get(position);

        imageView.setImageResource(rtaRdaValues.get(position).getRtaRdaId());
        manufacturerView.setText("Manufacturer: "+objectRtaRda.getRtaRdaManufacturer());
        nameView.setText("Name: "+objectRtaRda.getRtaRdaName());
        typeView.setText("Type: "+objectRtaRda.getRtaRdaType());
        coilsView.setText("Coils: "+Integer.toString(objectRtaRda.getRtaRdaCoils()));
        priceView.setText("Price: "+Double.toString(objectRtaRda.getRtaRdaPrice()));

        return convertView;
    }
}
