package com.example.myprogram.rtaRda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.myprogram.onClickButtons.OnClickCallButton;
import com.example.myprogram.R;

public class RtaRdaDescription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rta_rda_description);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        RtaRdaValues rtaRdaValues = intent.getExtras().getParcelable(RtaRdaModels.ITEM_KEY);

        FloatingActionButton fab = findViewById(R.id.contactInfoButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RtaRdaDescription.this, OnClickCallButton.class));
            }
        });

        TextView textView = findViewById(R.id.rta_rda_description_textView);

        getSupportActionBar().setTitle(rtaRdaValues.getRtaRdaName());

        textView.setText(rtaRdaValues.getRtaRdaDescription());

    }
}
