package com.example.myprogram.rtaRda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myprogram.R;
import com.example.myprogram.batteries.BatteriesValues;
import com.example.myprogram.boxModes.BoxModesKitsModels;
import com.example.myprogram.boxModes.BoxModesValues;

import java.util.ArrayList;
import java.util.List;

public class RtaRdaEdit extends AppCompatActivity {

//    private EditText boxManufacturer;
//    private EditText boxName;
//    private EditText boxPrice;
//    private EditText boxBatteries;
//    private EditText boxPower;
//
//    private Button submit;
//
//    private int photoResourseId;
//    private int descriptionId;
//
//    private List<BatteriesValues> list;
//
//
//
//    BoxModesValues boxModes;


    private EditText rtaRdaName;
    private EditText rtaRdaManufacturer;
    private EditText rtaRdaType;
    private EditText rtaRdaPrice;
    private EditText rtaRdaCoils;
    private Button submit;

    private int rtaRdaId;
    private int rtaRdaDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rta_rda_edit);


        rtaRdaName = findViewById(R.id.rtaRdaNameEdit);
        rtaRdaManufacturer =  findViewById(R.id.rtaRdaManufacturerEdit);
        rtaRdaType  = findViewById(R.id.rtaRdaTypeEdit);
        rtaRdaPrice = findViewById(R.id.rtaRdaPriceEdit);
        rtaRdaCoils = findViewById(R.id.rtaRdaCoilsEdit);
        submit = findViewById(R.id.submitChangesEdit);

        final Intent intent = getIntent();
        final RtaRdaValues thisRtaRda = intent.getExtras().getParcelable(RtaRdaModels.ITEM_KEY);

        rtaRdaName.setText(thisRtaRda.getRtaRdaName());
        rtaRdaManufacturer.setText(thisRtaRda.getRtaRdaManufacturer());
        rtaRdaType.setText(thisRtaRda.getRtaRdaType());
        rtaRdaPrice.setText("" + thisRtaRda.getRtaRdaPrice());
        rtaRdaCoils.setText("" + thisRtaRda.getRtaRdaCoils());
        rtaRdaId = thisRtaRda.getRtaRdaId();
        rtaRdaDescription = thisRtaRda.getRtaRdaDescription();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RtaRdaValues rtaRdaValues = new RtaRdaValues(rtaRdaName.getText().toString(),
                        rtaRdaManufacturer.getText().toString(),
                        rtaRdaType.getText().toString(),
                        Integer.parseInt(rtaRdaCoils.getText().toString()),
                        Double.parseDouble(rtaRdaPrice.getText().toString()),
                        R.drawable.your_photo_here);


                Intent intent_second = new Intent();
                intent_second.putExtra(RtaRdaModels.ITEM_KEY,rtaRdaValues);
                setResult(RESULT_OK,intent_second);

                onBackPressed();
            }
        });

    }
}
