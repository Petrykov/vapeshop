package com.example.myprogram.rtaRda;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.myprogram.R;

import java.util.ArrayList;
import java.util.List;

public class RtaRdaModels extends AppCompatActivity {

    public static final String DESCRIPTION_ITEM_KEY = "rta_rda_description_item_key";
    public static final String ITEM_KEY = "hello";
    public static final int ACTION_ADD_RTA_RDA= 115;
    public static final int ACTION_EDIT_RTA_RDA = 116;

    List <RtaRdaValues> rtaRdaValuesList;

    RtaRdaAdapter rtaRdaAdapter;

    private boolean attempToDelete = false;
    public  boolean attemptToEdit = false;

    private int positionDeletedItem = -1;
    private int editRtaRda;

    private RtaRdaValues deletedItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rta_rda_models);

        rtaRdaValuesList = new ArrayList<>();

        rtaRdaValuesList.add(new RtaRdaValues("Goon V1.5","Custom Vapes","RDA",2,35,R.drawable.goon,R.string.goon_description));
        rtaRdaValuesList.add(new RtaRdaValues("Apocalypse","USA","RDA",2,25,R.drawable.apocal,R.string.apocalypse_description));
        rtaRdaValuesList.add(new RtaRdaValues("Bonza","Malasya","RDA",2,20,R.drawable.bonza,R.string.bonza_description));
        rtaRdaValuesList.add(new RtaRdaValues("Kennedy","Russia","RDA",2,30,R.drawable.kennedy,R.string.kennedy_description));
        rtaRdaValuesList.add(new RtaRdaValues("Phobia","USA","RDA",2,20,R.drawable.phobia,R.string.phobia_description));
        rtaRdaValuesList.add(new RtaRdaValues("Reload","USA","RTA",2,40,R.drawable.reload_rta,R.string.reload_description));
        rtaRdaValuesList.add(new RtaRdaValues("Vgod","Usa","RDA",2,25,R.drawable.vgod,R.string.vogd_description));

        rtaRdaAdapter = new RtaRdaAdapter(this,rtaRdaValuesList);

        ListView rtaRdaListView = findViewById(R.id.rta_rda_listView);

        rtaRdaListView.setAdapter(rtaRdaAdapter);
        rtaRdaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (attempToDelete) {
                    deletedItem = rtaRdaValuesList.get(position);
                    positionDeletedItem = position;
                    rtaRdaValuesList.remove(position);
                    rtaRdaAdapter.notifyDataSetChanged();

                    Snackbar.make(view,"The " + deletedItem.getRtaRdaName() + " was successfully deleted!",Snackbar.LENGTH_LONG).setAction("Return", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rtaRdaValuesList.add(positionDeletedItem,deletedItem);
                            rtaRdaAdapter.notifyDataSetChanged();
                        }
                    }).show();

                    attempToDelete = false;
                }else {
                    if(attemptToEdit){
                        Intent intent = new Intent (RtaRdaModels.this, RtaRdaEdit.class);
                        intent.putExtra(ITEM_KEY,rtaRdaValuesList.get(position));
                        editRtaRda = position;
                        attemptToEdit = false;
                        startActivityForResult(intent,ACTION_EDIT_RTA_RDA);
                    }

                else{
                        Intent intent = new Intent(RtaRdaModels.this, RtaRdaDescription.class);
                        intent.putExtra(ITEM_KEY, rtaRdaValuesList.get(position));

                        startActivity(intent);
                    }
                }
            }
        });

        Button delete_button = findViewById(R.id.delete_button);

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to delete",Snackbar.LENGTH_LONG).show();
                attempToDelete = true;
            }
        });

        Button editButton = findViewById(R.id.delete_button_batteries);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Press on item to edit",Snackbar.LENGTH_LONG).show();
                attemptToEdit = true;
            }
        });

    }

    public void addRtaRda(View view){
        startActivityForResult(new Intent(this, AddRtaRda.class),ACTION_ADD_RTA_RDA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == ACTION_ADD_RTA_RDA & resultCode == RESULT_OK){
            RtaRdaValues rtaRdaValues = data.getExtras().getParcelable(ITEM_KEY);
            rtaRdaValuesList.add(rtaRdaValues);
            rtaRdaAdapter.notifyDataSetChanged();
        }else if (requestCode == ACTION_EDIT_RTA_RDA & resultCode == RESULT_OK){
            RtaRdaValues rtaRdaValues = data.getExtras().getParcelable(ITEM_KEY);
            rtaRdaValuesList.set(editRtaRda,rtaRdaValues);
            rtaRdaAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
