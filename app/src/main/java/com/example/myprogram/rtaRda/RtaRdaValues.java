package com.example.myprogram.rtaRda;

import android.os.Parcel;
import android.os.Parcelable;

public class RtaRdaValues implements Parcelable {

    private String rtaRdaName;
    private String rtaRdaManufacturer;
    private String rtaRdaType;

    private double rtaRdaPrice;

    private int rtaRdaCoils;
    private int rtaRdaId;
    private int rtaRdaDescription;

    public RtaRdaValues(String rtaRdaName, String rtaRdaManufacturer, String rtaRdaType, int rtaRdaCoils, double rtaRdaPrice, int rtaRdaId, int rtaRdaDescription) {
        this.rtaRdaName = rtaRdaName;
        this.rtaRdaManufacturer = rtaRdaManufacturer;
        this.rtaRdaType = rtaRdaType;
        this.rtaRdaCoils = rtaRdaCoils;
        this.rtaRdaPrice = rtaRdaPrice;
        this.rtaRdaId = rtaRdaId;
        this.rtaRdaDescription = rtaRdaDescription;
    }

    public RtaRdaValues(String rtaRdaName, String rtaRdaManufacturer, String rtaRdaType, int rtaRdaCoils, double rtaRdaPrice,int rtaRdaId){
        this.rtaRdaName = rtaRdaName;
        this.rtaRdaManufacturer = rtaRdaManufacturer;
        this.rtaRdaType = rtaRdaType;
        this.rtaRdaCoils = rtaRdaCoils;
        this.rtaRdaPrice = rtaRdaPrice;
        this.rtaRdaId = rtaRdaId;
    }

    public String getRtaRdaName() {
        return rtaRdaName;
    }

    public String getRtaRdaManufacturer() {
        return rtaRdaManufacturer;
    }

    public String getRtaRdaType() {
        return rtaRdaType;
    }

    public int getRtaRdaCoils() {
        return rtaRdaCoils;
    }

    public double getRtaRdaPrice() {
        return rtaRdaPrice;
    }

    public int getRtaRdaId() {
        return rtaRdaId;
    }

    public int getRtaRdaDescription() {
        return rtaRdaDescription;
    }

    public void setRtaRdaDescription(int rtaRdaDescription) {
        this.rtaRdaDescription = rtaRdaDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.rtaRdaName);
        dest.writeString(this.rtaRdaManufacturer);
        dest.writeString(this.rtaRdaType);
        dest.writeInt(this.rtaRdaCoils);
        dest.writeDouble(this.rtaRdaPrice);
        dest.writeInt(this.rtaRdaId);
        dest.writeInt(this.rtaRdaDescription);
    }

    protected RtaRdaValues(Parcel in) {
        this.rtaRdaName = in.readString();
        this.rtaRdaManufacturer = in.readString();
        this.rtaRdaType = in.readString();
        this.rtaRdaCoils = in.readInt();
        this.rtaRdaPrice = in.readDouble();
        this.rtaRdaId = in.readInt();
        this.rtaRdaDescription = in.readInt();
    }

    public static final Parcelable.Creator<RtaRdaValues> CREATOR = new Parcelable.Creator<RtaRdaValues>() {
        @Override
        public RtaRdaValues createFromParcel(Parcel source) {
            return new RtaRdaValues(source);
        }

        @Override
        public RtaRdaValues[] newArray(int size) {
            return new RtaRdaValues[size];
        }
    };
}
